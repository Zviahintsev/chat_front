import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Provider, connect}   from 'react-redux';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';  
import { GraphQLClient } from 'graphql-request'

const gql = new GraphQLClient("http://localhost:3001 /graphql", { headers: {} })

const sendReducer = (state, action) => {
  if (state === undefined || action.type === 'SEND_DEFAULT')
      return {status: 'DEFAULT'}
  if (action.type === 'SEND')
      return {status: 'SEND'}
  if (action.type === 'SENT')
      return {status: 'SENT'}
  if (action.type === 'SEND_ERROR')
      return {status: 'ERROR'}
  return state;
}

const messagesReducer = (state, action) => {
  if (state === undefined || action.type === 'MESSAGES_DEFAULT')
      return {messages: [], status: 'RESOLVED'}
  if (action.type === 'MESSAGES_PENDING')
      return {messages: state.messages, status: 'PENDING'}
  if (action.type === 'MESSAGES_RESOLVED')
      return {messages: [...action.messages,...state.messages], status: 'RESOLVED'}
  if (action.type === 'MESSAGES_ERROR')
      return {messages: state.messages, status: 'ERROR'}
  return state;
}
const chatReducer = (state, action) => {
    if (state = undefined  || action.type === 'CHAT_CLEAR')
        return {status: 'CLEAR'}
    if (action.type === 'CHAT_MESSAGES')
        return {messages:[...action.messages,...state.messages], status:'RESOLVED'}
}
const reducers = combineReducers({
  send: sendReducer,
  messages: messagesReducer,
  chat: chatReducer
})

const store = createStore(reducers, applyMiddleware(thunk))

let sendDefault = () => ({type: 'SEND_DEFAULT'})
let sendError = () => ({type: 'SEND_ERROR'})
let sendSend = () => ({type: 'SEND'})
let sendSent = () => ({type: 'SENT'})



let delay = ms => new Promise(r => setTimeout(r, ms))

let send = (nick, message) => 
  async dispatch => {
      dispatch(sendSend())
      try {
          let result = await 
              jsonPost("http://students.a-level.com.ua:10012", {func: 'addMessage', nick, message})
          dispatch(sendSent())
      }
      catch (e) {
          console.log(e)
          dispatch(sendError())
      }
      finally {
          dispatch(getMessages())
          await delay(2000)
          dispatch(sendDefault())
      }
  }

let messagePending = () => ({type: 'MESSAGES_PENDING'})
let messageResolved = messages => ({type: 'MESSAGES_RESOLVED', messages})
let messageError = () => ({type: 'MESSAGES_ERROR'})

let getMessages = () =>
  async dispatch => {
      dispatch(messagePending())
      try {
          debugger;
          let result = await 
              jsonPost("http://students.a-level.com.ua:10012", {func: 'getMessages', messageId: store.getState().messages.messages.length})
          console.log('new messages:', result.data.length)
          dispatch(messageResolved(result.data.reverse()))
      }
      catch (e){
          dispatch(messageError())
      }
  }

store.subscribe(()=> console.log(store.getState())) // подписка на обновления store


// function jsonPost(url, data)
// {
//   return new Promise((resolve, reject) => {
//       var x = new XMLHttpRequest();   
//       x.onerror = () => reject(new Error('jsonPost failed'))
//       x.open("POST", url, true);
//       x.send(JSON.stringify(data))

//       x.onreadystatechange = () => {
//           if (x.readyState == XMLHttpRequest.DONE && x.status == 200){
//               resolve(JSON.parse(x.responseText))
//           }
//           else if (x.status != 200){
//               reject(new Error('status is not 200'))
//           }
//       }
//   })
// }

class Inputs extends Component {
  constructor(props){
      super(props)

      this.send = this.send.bind(this)
  }
  send(){
      if (typeof this.props.onSend === 'function'){
          this.props.onSend(this.nick.value, this.message.value)
      }
  }
  render(){
      console.log(this.props)
      if (this.message && this.props.status === 'SENT'){
          this.message.value = ''
      }
      let messageStyle = {backgroundColor: ({DEFAULT: "", 
                                            SENT: "lightgreen",
                                            ERROR: 'red'})[this.props.status]}
      return (
          <div>
              <input placeholder='nick' ref={c => this.nick = c}/>
              <input placeholder='message' style={messageStyle} ref={c => this.message = c} disabled={this.props.status === 'SEND'} />
              <button onClick={this.send} disabled={this.props.status === 'SEND'}>Send</button>
          </div>
      )
  }
}

Inputs = connect(state => ({status: state.send.status}), {onSend: send})(Inputs)

let ChatMessage = p =>
<div>
  <b>{p.msg.nick}</b>:
  <span>{p.msg.message}</span>:
</div>

let Chat = p => {
  console.log(p)
return (
  <div>
  {p.messages.map(msg => <ChatMessage msg={msg} key={Math.random()} />)}
  </div>
)
}

Chat = connect(state => ({messages: state.messages.messages}))(Chat)

async function getMessagesLoop(){
  while(true){
      store.dispatch(getMessages())
      await delay(1000)
  }
}

getMessagesLoop()

gql.request(`query getMessages($id: String!){
    getMessages(id:$id){
          message
        }
      }`, {postID: this.props.match.params.id}) 
.then(data => store.dispatch({type: "DATA", data}))

class App extends Component {
render() {
  
  return (
      <Provider store = {store} >
        <div className="App">
          <Inputs />
          <Chat messages={[]}/>
        </div>
      </Provider>
  );
}
}

export default App;
